/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
    var minDiscount = 100;          
    var maxDiscount = 200;
    var daysInSale = 30;
    var initialPrice = 1000;
    var firstDay = moment("23-06-2014", "DD-MM-YYYY");
    
    init();
    doTheJob();

    /*
     * 
     * Function to initialize the datepicker with the 
     * defaul day
     */
    function init() {
        $('#txtInitialDay').datepicker({dateFormat: 'dd-mm-yy', maxDate: new Date()});
        $('#txtInitialDay').datepicker('setDate', firstDay.toDate());
    }
    
    
    /*
     * 
     * Function to populate the 'sale panel'
     */
    function doTheJob() {
        // Today
        var now = moment();

        // Days since the start of sale
        var days = now.diff(firstDay, "day");

        var daysLeft = daysInSale - days;
        if( daysLeft >= 0 ) {
            // Calculate discount
            var actualDiscount = minDiscount + (days * (maxDiscount - minDiscount)) / daysInSale;
            // Round to second decimal
            actualDiscount = Math.round(actualDiscount * 100) / 100;
            // Fill up the panel header
            if(daysLeft == 1) $('#sale-data').text( daysLeft + ' day left!!');
            else
                $('#sale-data').text( daysLeft + ' days left!!')

            // Fill up with new price
            $('#sale-img').html('<span id="img">now <br />' + (initialPrice - actualDiscount) + '$</span>');
            // Show initial price
            $('#sale-desc').html('<span class="label label-success pull-right">Was ' + initialPrice + '$</span>');
            // Setup progress bar
            $('.progress-bar').attr("aria-valuenow", actualDiscount);
            $('.progress-bar').attr("aria-valuemin", minDiscount);
            $('.progress-bar').attr("aria-valuemax", maxDiscount);
            $('.progress-bar').attr("style", 'width:' + Math.round(((actualDiscount - minDiscount) * 100) / 100 ) + '%' );
            $('.progress-bar').text(actualDiscount + '$');
        }else {
            // Out of sale
            $('#sale-data').text('Sale expired!!')
            $('#sale-img').html('<span id="img">SOLD <br />OUT</span>');
            // Setup progress bar
            $('.progress-bar').attr("aria-valuenow", actualDiscount);
            $('.progress-bar').attr("aria-valuemin", minDiscount);
            $('.progress-bar').attr("aria-valuemax", maxDiscount);
            $('.progress-bar').attr("style", 'width: 0%' );
            $('.progress-bar').text('0$');
        }
    }
    
    /**
     *  Setup the submit button of the form
     */
    
    $('#frmUpdate').submit(function(){
        firstDay = $('#txtInitialDay').datepicker('getDate');
        doTheJob();
        return false;
    });
});